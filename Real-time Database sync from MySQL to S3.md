# Real-time Database sync from MySQL to S3

To Create a real time data sync we need to create a kafka cluster and add debezium in it.

## Steps to create Kafka Cluster

Create three EC2 instances with the same configration. 
### EC2 Setup

Launch 3 ubuntu instance with t2.medium instance type and a memory of 10 GB  Persistent SSD Disks.


![Select AMI](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/select%20ami.png)

Select an AMI


![Select Instance Type](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/select_instance.png)


Select Instance Type

![Select VPC and IAM Roles](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/addition%20settings.png)



Select VPC and IAM Roles

![Add Storage](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/storage.png)


Add Storage

![Add Name for the instance](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/name.png)

Add Name for the instance

![Select Security group](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/sg.png)


Select Security group

![Select Key Pair](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/keypair.png)


Select Key Pair

![Lanch Template](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/Lanchtemp.png)


Launch Rest of the instances using a Template.

### Kafka Setup

First go to the security group and add access to the ip's given below.

![Add IP's to SG](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/sg_ips.PNG)

Login into the ec2 instance and run the following commands.

``` bash
apt-get -y update 
sudo apt-get -y install default-jre
```

``` bash
wget -qO - https://packages.confluent.io/deb/5.3/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/5.3 stable main"
sudo apt-get update && sudo apt-get install confluent-platform-2.12
```
Then Run the following command to open the zookeeper properties file and then add the following configrations.


``` bash
mkdir zookeeper
mkdir kafka
```
```
vim \etc\kafka\zookeeper.properties
```
```
-- On Node 1
dataDir=/home/ubuntu/zookeeper
clientPort=2181
maxClientCnxns=0
server.1=0.0.0.0:2888:3888
server.2=10.22.1.177:2888:3888
server.3=10.22.1.112.207:2888:3888
autopurge.snapRetainCount=3
autopurge.purgeInterval=24
initLimit=5
syncLimit=2

-- On Node 2
dataDir=/home/ubuntu/zookeeper
clientPort=2181
maxClientCnxns=0
server.1=10.22.1.92:2888:3888
server.2=0.0.0.0:2888:3888
server.3=10.22.1.112:2888:3888
autopurge.snapRetainCount=3
autopurge.purgeInterval=24
initLimit=5
syncLimit=2

-- On Node 3
dataDir=/home/ubuntu/zookeeper
clientPort=2181
maxClientCnxns=0
server.1=10.22.1.92:2888:3888
server.2=10.22.1.177:2888:3888
server.3=0.0.0.0:2888:3888
autopurge.snapRetainCount=3
autopurge.purgeInterval=24
initLimit=5
syncLimit=2

```
Run the follow  commands for adding the node id
``` bash
 -- On Node 1
 echo "1" > /home/ubuntu/zookeeper/myid
 
 --On Node 2
 echo "2" > /home/ubuntu/zookeeper/myid
 
 --On Node 3
 echo "3" > /home/ubuntu/zookeeper/myid
```
Run the following command and add the folloing properties to tun the kafka nodes.
``` bash
vim /etc/kafka/server.properties
```

```
--On Node 1
broker.id.generation.enable=true
delete.topic.enable=true
listeners=PLAINTEXT://:9092
zookeeper.connect=10.22.1.112:2181,10.22.1.92:2181,10.22.1.177:2181 
log.dirs=/home/ubuntu/kafka
log.retention.hours=168
num.partitions=1

--On Node 2
broker.id.generation.enable=true
delete.topic.enable=true
listeners=PLAINTEXT://:9092
log.dirs=/home/ubuntu/kafka
zookeeper.connect=10.22.1.112:2181,10.22.1.92:2181,10.22.1.177:2181 
log.retention.hours=168
num.partitions=1

-- On Node 3
broker.id.generation.enable=true
delete.topic.enable=true
listeners=PLAINTEXT://:9092
log.dirs=/home/ubuntu/kafka
zookeeper.connect=10.22.1.112:2181,10.22.1.92:2181,10.22.1.177:2181 
num.partitions=1
log.retention.hours=168
```

To increase the Java JVM Heap size run the following command and change the configration of heap size to 2GB for kafka node.

``` bash
vim /usr/bin/kafka-server-start
```

```
export KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"
```

run the following commands to check if kafka and zookeeper are running

``` bash 
zookeeper-serever-start /etc/kafka/zookeeper.properties
kafka-server-start /etc/kafka/server.properties
```

To stop the server
``` bash
zookeeper-server-stop
kafka-server-stop
```
Create a servic file with the following command and add the following configrations
``` bash
vim /etc/systemd/system/kafka.service
```
Add path for java home for your current version.
``` bash
[Unit]
Description=Apache Kafka Server
Documentation=http://kafka.apache.org/documentation.html   
StartLimitBurst=5
StartLimitIntervalSec=33  

[Service]   
Type=simple     
ExecStartPre=/bin/sleep 6                                                          
Environment="JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64"
ExecStart=/usr/bin/kafka-server-start /etc/kafka/server.properties
ExecStop=/usr/bin/kafka-server-stop  
Restart=on-failure
RestartSec=5     

[Install]   
WantedBy=multi-user.target
```
``` bash
vim /etc/systemd/system/zookeeper.service
```
``` bash

[Unit]
Description=Apache Zookeeper server
Documentation=http://zookeeper.apache.org
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
ExecStart=/usr/bin/zookeeper-server-start /etc/kafka/zookeeper.properties
ExecStop=/usr/bin/zookeeper-server-stop
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```
Then run the following commands to set auto start for zookeeper and kafka when system is restarted.

``` bash 
sudo systemctl enable zookeeper
sudo systemctl enable kafka
```
Then start the kafka and zookeeper server.

``` bash 
sudo systemctl start zookeeper
sudo systemctl start kafka
```
Run the following command to check if everything is running properly

``` bash
zookeeper-shell 10.22.1.112:2181 ls /brokers/ids
```
You should get the following output.


![zookeeper shell output](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/zookeeper_shell.png)

Use the command to list kafka topics
``` bash
kafka-topics --list --zookeeper localhost:2181
```

## Add Debezium

Install the confluent connector and debezium MySQL connector on all the producer nodes.

``` bash
apt-get update 
sudo apt-get install default-jre
```

``` bash
wget -qO - https://packages.confluent.io/deb/5.3/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/5.3 stable main"
sudo apt-get update && sudo apt-get install confluent-hub-client confluent-common confluent-kafka-connect-s3 confluent-kafka-2.12
```
Run the command and add the following properties to the file on all connector nodes.
``` bash
vim /etc/kafka/connect-distributed.properties
```

``` bash
bootstrap.servers=10.22.1.112:9092,10.22.1.92:9092,10.22.1.177:9092
group.id=debezium-cluster
plugin.path=/usr/share/java,/usr/share/confluent-hub-components
```

### Install Debezium MySQL Connector

Run the following command to install the mysql connector and enter `Y` for every option.
``` bash
confluent-hub install debezium/debezium-connector-mysql:latest
```
Create a service for starting the connector 

``` bash
vi /lib/systemd/system/confluent-connect-distributed.service
```
``` bash
[Unit]
Description=Apache Kafka - connect-distributed
Documentation=http://docs.confluent.io/
After=network.target

[Service]
Type=simple
User=cp-kafka
Group=confluent
ExecStartPre=/bin/sleep 12 
ExecStart=/usr/bin/connect-distributed /etc/kafka/connect-distributed.properties
TimeoutStopSec=180
Restart=no

[Install]
WantedBy=multi-user.target
```

``` bash
systemctl enable confluent-connect-distributed
systemctl start confluent-connect-distributed
```
### Install MySQL Server
Install MySQL on one of the EC2 Instances.

``` bash
sudo apt-get update
sudo apt-get install mysql-server
```
```
server-id               = 1
log_bin                 = /var/log/mysql/mysql-bin.log
```
Open mysql command line.
``` bash
service mysql start
mysql
```
``` sql
CREATE USER 'rahul'@'%' IDENTIFIED BY 'my_password';
GRANT ALL PRIVILEGES ON *.* TO 'rahul'@'10.22.1.112' WITH GRANT OPTION;
```
``` bash
vim /etc/mysql/mysql.conf.d/mysqld.cnf 
```
Add the following configrations


### Configure Debezium MySQL Connector


Create a mysql.json file which contains the MySQL information and other formatting options.
``` bash
vim mysql.json
```

``` json
{
        "name": "mysql-connector-db01",
        "config": {
                "name": "mysql-connector-db01",
                "connector.class": "io.debezium.connector.mysql.MySqlConnector",
                "database.server.id": "1",
                "tasks.max": "3",
                "database.history.kafka.bootstrap.servers": "10.22.1.112:9092,10.22.1.92:9092,10.22.1.177:9092",
                "database.history.kafka.topic": "new-schema-changes.mysql",
                "database.server.name": "mysql-db01",
                "database.hostname": "10.22.1.112",
                "database.port": "3306",
                "database.user": "mysql-test",
                "database.password": "my_password",
                "database.whitelist": "proddb,test",
                "database.allowPublicKeyRetrieval":true,
                "internal.key.converter.schemas.enable": "false",
                "key.converter.schemas.enable": "false",
                "internal.key.converter": "org.apache.kafka.connect.json.JsonConverter",
                "internal.value.converter.schemas.enable": "false",
                "value.converter.schemas.enable": "false",
                "internal.value.converter": "org.apache.kafka.connect.json.JsonConverter",
                "value.converter": "org.apache.kafka.connect.json.JsonConverter",
                "key.converter": "org.apache.kafka.connect.json.JsonConverter",
                "snapshot.mode": "initial",
                "transforms": "unwrap",
                "transforms.unwrap.type": "io.debezium.transforms.ExtractNewRecordState",
        "transforms.unwrap.add.source.fields": "ts_ms",
                "tombstones.on.delete": false
        }
}
```

``` bash
curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8083/connectors -d @mysql.json
curl GET localhost:8083/connectors/mysql-connector-db01/status
```

Output for the status command
``` json
{
  "name": "mysql-connector-db01",
  "connector": {
    "state": "RUNNING",
    "worker_id": "10.22.1.92:8083"
  },
  "tasks": [
    {
      "id": 0,
      "state": "RUNNING",
      "worker_id": "10.22.1.112:8083"
    }
  ],
  "type": "source"
}
```
Create a test databse and create a table and insert rows in the table.
``` sql
use test;
create table rohi (id int,
fn varchar(10),
ln varchar(10),
phone int );

insert into rohi values (2, 'rohit', 'ayare','87611');
```
Use this command to check if the test topic is appearing or not
``` bash
kafka-topics --list --zookeeper localhost:2181
```
This topic should appear in the output.
```
mysql-db01.test.rohi
```
Check if the data that is entered in the mysql table is appearing in the topic.
``` bash
kafka-console-consumer --bootstrap-server localhost:9092 --from-beginning --topic mysql-db01.test.rohi
```

The output should be somewhat ilke this.
``` json
{"id":1,"fn":"rohit","ln":"ayare","phone":87611,"__ts_ms":1576757407000}
```

### Setup S3 Sink connector
Attach an IAM Role to EC2 Instance to access S3

Create s3.json file.

``` json

{
        "name": "s3-sink-db01",
        "config": {
                "connector.class": "io.confluent.connect.s3.S3SinkConnector",
                "storage.class": "io.confluent.connect.s3.storage.S3Storage",
                "s3.bucket.name": "test-dharmil",
                "name": "s3-sink-db01",
                "tasks.max": "3",
                "s3.region": "us-east-1",
                "s3.part.size": "5242880",
                "s3.compression.type": "gzip",
                "timezone": "UTC",
                "locale": "en",
                "flush.size": "4",
                "rotate.interval.ms": "3600000",
                "topics.regex": "mysql-db01.(.*)",
                "internal.key.converter.schemas.enable": "false",
                "key.converter.schemas.enable": "false",
                "internal.key.converter": "org.apache.kafka.connect.json.JsonConverter",
                "format.class": "io.confluent.connect.s3.format.json.JsonFormat",
                "internal.value.converter.schemas.enable": "false",
                "value.converter.schemas.enable": "false",
                "internal.value.converter": "org.apache.kafka.connect.json.JsonConverter",
                "value.converter": "org.apache.kafka.connect.json.JsonConverter",
                "key.converter": "org.apache.kafka.connect.json.JsonConverter",
                "partitioner.class": "io.confluent.connect.storage.partitioner.HourlyPartitioner",
                "path.format": "YYYY/MM/dd/HH",
                "partition.duration.ms": "3600000",
                "rotate.schedule.interval.ms": "3600000"
        }
}
```
`flush.size` - The data will uploaded to S3 only after these many number of records stored.
``` bash
curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:8083/connectors -d @s3.json
curl GET localhost:8083/connectors/s3-sink-db01/status
```


``` json

{
  "name": "s3-sink-db01",
  "connector": {
    "state": "RUNNING",
    "worker_id": "172.31.94.191:8083"
  },
  "tasks": [
    {
      "id": 0,
      "state": "RUNNING",
      "worker_id": "172.31.94.191:8083"
    },
    {
      "id": 1,
      "state": "RUNNING",
      "worker_id": "172.31.94.191:8083"
    },
    {
      "id": 2,
      "state": "RUNNING",
      "worker_id": "172.31.94.191:8083"
    }
  ],
  "type": "sink"
}
```

### Test the S3 sync

Insert the 4 rows into the rohi table. Then check the S3 bucket. It’ll save the data in JSON format with GZIP compression. Also in a HOUR wise partitions.


![zookeeper shell output](https://gitlab.com/rahul.narisetti/test2/-/raw/master/Images/s3_output.PNG)

